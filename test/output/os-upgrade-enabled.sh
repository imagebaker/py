#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
echo "info - started upgrading default operating system ..."
apt-get update
apt-get upgrade -y
rm -rf /var/lib/apt/lists/*
echo "info - finished upgrading default operating system ..."