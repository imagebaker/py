import yaml
import subprocess
import sys
import os


# set the default output directory
output_directory = "../test/output/"

# open the configuraion file
with open('../test/team/mars.yaml', 'r') as file:
    team_yaml = yaml.safe_load(file)

# parsing values
team_name = team_yaml['team']['name']
container_image = team_yaml['container_image']
post_build_script_enabled = team_yaml['container_image']['post_build']['is_enabled']
post_build_script = team_yaml['container_image']['post_build']['commands']
os_distribution = team_yaml['container_image']['os']['distribution']
os_version = team_yaml['container_image']['os']['version']
os_packages = team_yaml['container_image']['os']['os_packages']
system_variables_enabled = team_yaml['container_image']['system_variables']['is_enabled']
system_variables = team_yaml['container_image']['system_variables']['variables']
os_force_update_enabled = team_yaml['container_image']['os']['force_os_update']
py_enabled = team_yaml['container_image']['components']['python']['is_enabled']
py_packages = team_yaml['container_image']['components']['python']['py_packages']
py_versions_list = team_yaml['container_image']['components']['python']['versions']

# validate yaml
# still todo


print(py_versions_list)

# write system variables into a file
if system_variables_enabled == "true":
    with open((output_directory + 'system-variables.sh'), mode='wt', encoding='utf-8') as system_variables_file:
        for key, value in system_variables.items():
            system_variables_file.write('export %s="%s"\n' % (key, value))
else:
    print("info - no system variables set, skipping ...")


# write post-build into a file
if post_build_script_enabled == "true":
    with open((output_directory + 'post-build.sh.txt'), mode='wt', encoding='utf-8') as post_build_script_file:
        post_build_script_file.write((post_build_script).strip())
else:
    print("info - no system variables set, skipping ...")


# write output file for OS packages
with open((output_directory + 'os-package-list.txt'), mode='wt', encoding='utf-8') as os_requirements:
    os_requirements.write((os_packages).strip())

# write output file for Python packages
if py_enabled == "true":
    with open((output_directory + 'requirements-.txt'), mode='wt', encoding='utf-8') as py_requirements_file:
        py_requirements_file.write((py_packages).strip())

# write output file for Python version
if py_enabled == "true":
    with open((output_directory + 'python-versions.txt'), mode='wt', encoding='utf-8') as py_versions_file:
        py_versions_file.write('\n'.join(py_versions_list))


# write script to force update os the OS
if os_force_update_enabled == "true":
    
    # for Ubuntu only
    if os_distribution == "ubuntu":
        ubuntu_update_script = """
#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
echo "info - started upgrading default operating system ..."
apt-get update
apt-get upgrade -y
rm -rf /var/lib/apt/lists/*
echo "info - finished upgrading default operating system ..."
"""

        # The default umask is 0o22 which turns off write permission of group and others
        os.umask(0)

        bash_file_descriptor = os.open(
            path=(output_directory + 'os-upgrade-enabled.sh'),
            flags=(
                os.O_WRONLY  # access mode: write only
                | os.O_CREAT  # create if not exists
                | os.O_TRUNC  # truncate the file to zero
            ),
            mode=0o755
        )

        with open(bash_file_descriptor,"w") as bash_script:
            bash_script.write((ubuntu_update_script).strip())
    # for CentOS only
    elif os_distribution == "centos":
        centos_update_script = """
#!/bin/bash
echo "info - started upgrading default operating system ..."
yum -y update
yum clean all && \
rm -rf /var/cache/yum
echo "info - finished upgrading default operating system ..."
"""

        # The default umask is 0o22 which turns off write permission of group and others
        os.umask(0)

        bash_file_descriptor = os.open(
            path=(output_directory + 'os-upgrade-enabled.sh'),
            flags=(
                os.O_WRONLY  # access mode: write only
                | os.O_CREAT  # create if not exists
                | os.O_TRUNC  # truncate the file to zero
            ),
            mode=0o755
        )
        with open(bash_file_descriptor,"w") as bash_script:
            bash_script.write((centos_update_script.strip()))
    else:
        print("error - no compatible OS distribution.")

